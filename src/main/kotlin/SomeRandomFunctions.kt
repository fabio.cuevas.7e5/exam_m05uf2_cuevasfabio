import kotlin.math.abs

/**
 * Se crea una lista con los números que hay entre los dos introducidos en orden descendente.
 * @param[secondValue] Primer número de la lista
 * @param[secondValue] Segundo número de la lista
 * @return Lista de los números que hay entre m y n, incluyéndolos.
 */
//input m=7, n=3 output 7 6 5 4 3
fun numbersInBetween(firstValue: Int, secondValue: Int): List<Int> {
    val numero = mutableListOf<Int>()
    for (i in secondValue downTo firstValue) {
        numero.add(i)
    }
    return numero
}

/**
 * Devuelve una lista con los valores absolutos de la lista introducida.
 * @param[noAbsList] Lista con que puede contener valores positivos o negativos.
 * @return Lista con el valor absoluto de cada elemento de la lista(mismo número en caso de que sea positivo, número en positivo en caso de ser negativo).
 */
fun listAbsValues(noAbsList: List<Int>): List<Int> {
    return noAbsList.map{ abs(it) }
}

/**
 * Devuelve un string que califica la nota según el número introducido.
 * @param[nota] Nota del alumno
 * @return Resultado en String de la nota.
 */
// S'espera el seguent comportament
//  0: No presentat
//  1-4: Insuficient
//  5-6: Suficient
//  7-8: Notable
//  9: Excel·lent
//  10: Excel·lent + MH
fun gradeToString(nota: Int): String {
    //TODO: Cahotic order. Make it easy, only one return.
    when (nota) {
        1, 2, 3, 4 -> return "Insuficient"
        0 -> return "No presentat"
        10 -> return "Excelent + MH"
        5, 6 -> return "Suficient"
        7 -> return "Notable"
        else -> return "No valid"
    }
}

/**
 * Devuelve la cantidad de veces que cada letra del -String- es igual al -char-.
 * @param[string] String
 * @param[char] Caracter del que queremos saber cuantas veces se encuentra en el string
 */
fun CharOccurencesInString(string: String, char: Char): Int {
    var count = 1
    for (i in 0 until string.lastIndex) {
        if (string[i] == char) {
            count++
        }
    }
    return count
}

/**
 * Devuelve la posición de la última vez que -char- se encuentra en -string-.
 * @param[string] String
 * @param[char] Caracter del que queremos saber la posición de su última presencia en el string.
 */
//abc="marieta" b='a' -> 6
//abc="marieta" b='b' -> -1
fun charLastOccurrenceInString(string: String, char: Char): Int {
    val pos: MutableList<Int> = mutableListOf()
    for (i in string.lastIndex downTo 0) {
        if (string[i] == char) {
            return i
        }
    }
    return -1
}
/**
 * Redondea hacia el número de mayor valor si la nota es mayor que 5 y hacia el de menor valor si la nota es menor de 5.
 * @param[grade] Nota del estudiante que se desea redondear.
 * @return Nota redondeada.
 */
/*
La funció ha de retornar la nota arredonida. 8.7 -> 9, 7.5 -> 8
Excepció, Si la nota no arriba a un 5, no obtindrà el 5. És a dir: 4.9 -> 4
 */
fun gradeRoundOff(grade: Double): Int {
    return grade.toInt()
}