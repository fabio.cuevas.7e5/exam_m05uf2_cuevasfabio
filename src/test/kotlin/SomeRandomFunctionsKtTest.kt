import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class SomeRandomFunctionsKtTest {

    @Test
    fun numbersInBetweenTest() {
        val values = numbersInBetween(5, 10)
        val expected = listOf<Int>(5,6,7,8,9,10)
        assertEquals(expected, values)
    }
    @Test
    fun numbersNotBetweenValuesTest() {
        val actual = numbersInBetween(5,10)
        val list = listOf<Int>(5,6,7,8,9,10,11,12)
        assertEquals(list, actual)
    }

    @Test
    fun numbersBetweenValuesTestButBackwards() {
        val actual = numbersInBetween(10,5)
        val list = listOf<Int>(5,6,7,8,9,10)
        assertEquals(list, actual)
    }
    @Test
    fun numbersBetweenValuesTestButListIsBackwards() {
        val actual = numbersInBetween(10,5)
        val list = listOf<Int>(10,9,8,7,6,5)
        assertEquals(list, actual)
    }

    @Test
    fun gradeToStringTest() {
        val grade = 0
        val gradeToString = gradeToString(grade)
        assertEquals("No presentat", gradeToString)
    }
    @Test
    fun gradeToDifferentString() {
        val grade = 0
        val gradeToString = gradeToString(grade)
        assertEquals("Insuficient", gradeToString)
    }

    @Test
    fun differentGradeToString() {
        val grade = 8
        val gradeToString = gradeToString(grade)
        assertEquals("Insuficient", gradeToString)
    }

    @Test
    fun CharOccurencesInStringTest() {
        val string = "pruebaa"
        val char = 'a'
        val occurrences = CharOccurencesInString(string,char)
        val toBeExpected = 2
        assertEquals(toBeExpected, occurrences)
    }

    @Test
    fun GradeIsNotValid() {
        val grade = 23
        val gradeToString = gradeToString(grade)
        assertEquals("No valid", gradeToString)
    }

    @Test
    fun charLastOccurrenceInStringTest() {
        val string = "pruebaa"
        val char = 'a'
        val lastOccurrencePosition = charLastOccurrenceInString(string,char)
        val toBeExpected = (6)
        assertEquals(toBeExpected, lastOccurrencePosition)
    }
    @Test
    fun charLastOccurrenceInStringTestButNoOccurrence() {
        val string = "pruebaa"
        val char = 'h'
        val lastOccurrencePosition = charLastOccurrenceInString(string, char)
        val toBeExpected = 2
        assertEquals(toBeExpected, lastOccurrencePosition)
    }


    @Test
    fun roundOffTestButUp() {
        val grade = 5.8
        val roundOff = gradeRoundOff(grade)
        val toBeExpected = 6
        assertEquals(toBeExpected, roundOff)
    }

    @Test
    fun roundOffTest() {
        val grade = 5.8
        val roundOff = gradeRoundOff(grade)
        val toBeExpected = 5
        assertEquals(toBeExpected, roundOff)
    }

    @Test
    fun roundUpTestBelow5() {
        val grade = 4.8
        val roundOff = gradeRoundOff(grade)
        val toBeExpected = 5
        assertEquals(toBeExpected, roundOff)
    }

    @Test
    fun roundOffTestBelow5() {
        val grade = 4.8
        val roundOff = gradeRoundOff(grade)
        val toBeExpected = 4
        assertEquals(toBeExpected, roundOff)
    }
    @Test
    fun listAbsValuesTest(){
        val lista = listOf<Int>(2, -3, 4)
        val absValueConversion = listAbsValues(lista)
        val expected = listOf<Int>(2, 3, 4)
        assertEquals(expected, absValueConversion)
    }

}